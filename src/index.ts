import * as parser from "fast-xml-parser";
import * as fs from "fs";
import * as path from "path";

type DefinedObject = null | boolean | number | string | {
    [key: number]: DefinedObject;
} | {
    [key: string]: DefinedObject;
};
type RmsDtoArray = {
    readonly i: DefinedObject | Array<DefinedObject>
};
type RmsDtoMap = {
    readonly k: DefinedObject | Array<DefinedObject>;
    readonly v: DefinedObject | Array<DefinedObject>;
};
type RmsDtoAttributes = {
    cls: string;
    eid: string;
    null: number;
}
type RmsDtoObjWithAttributes = {
    __attributes: RmsDtoAttributes,
}
type RmsDtoObjWithValue = {
    __value: string,
}
type RmsDtoObj = RmsDtoObjWithAttributes & Partial<RmsDtoObjWithValue>;
type AttributesKey = keyof RmsDtoObjWithAttributes;
type ValueKey = keyof RmsDtoObjWithValue;
type XmlParserResultValue = boolean | number | string |
    Array<DefinedObject> | RmsDtoArray | RmsDtoMap | RmsDtoObj;

const attributesKey: AttributesKey = "__attributes";
const valueKey: ValueKey = "__value";

class RmsArray<T> extends Array<T> { }
class RmsMap<TKey, TValue> extends Map<TKey, TValue> { }
type RmsObjectInfo = {
    attributes: RmsDtoAttributes,
    byValue: boolean
}
function createRmsObj<T>(attr: RmsDtoAttributes, byValue: boolean, value: T): T {
    const proto = Object.assign({}, { rmsObj: true, attributes: attr, byValue: byValue } as RmsObjectInfo);
    const obj = Object.assign(Object.create(proto), value);
    return obj;
}
function getRmsProto(target: RmsDtoObj): RmsObjectInfo | null {
    const proto = Object.getPrototypeOf(target);
    return proto.rmsObj ? proto : null;
}
const xmlToJsOptions: parser.X2jOptionsOptional = {
    attributeNamePrefix: "",
    attrNodeName: attributesKey,
    textNodeName: valueKey,
    ignoreAttributes: false,
    ignoreNameSpace: false,
    allowBooleanAttributes: false,
    parseNodeValue: true,
    parseAttributeValue: true,
    trimValues: true,
    cdataTagName: "",
    cdataPositionChar: "",
    localeRange: "",
    parseTrueNumberOnly: false,
    arrayMode: false,
    attrValueProcessor: (value: string, attrName: string) => value,
    tagValueProcessor: (value: string, tagName: string) => value,
};
const jsToXmlOptions: parser.J2xOptions = {
    attributeNamePrefix: "",
    attrNodeName: attributesKey,
    textNodeName: valueKey,
    ignoreAttributes: true,
    cdataTagName: "",
    cdataPositionChar: "",
    format: true,
    indentBy: "  ",
    supressEmptyNode: true,
    tagValueProcessor: (value: string) => value,
    attrValueProcessor: (value: string) => value,
}
class ObjectProcessor {
    private readonly replacer: (value: any) => any;
    private readonly ignoredKeys: Set<string>;
    constructor(replacer: (value: any) => any, ignore?: Array<string>) {
        this.replacer = replacer;
        this.ignoredKeys = new Set(ignore);
    }
    public process(target: any): any {
        const v = this.getValue(target);
        return v;
    }
    private getValue(element: any): any {
        const r = this.replacer(
            typeof(element) === "object"
                ? this.walk(element)
                : element
        );
        return r;
    }
    private walk(target: any): any {
        if (target === null) {
            return null;
        } 
        if (Array.isArray(target)) {
            const result = new Array();
            for (const item of target) {
                const value = this.getValue(item)
                if (value !== undefined) {
                    result.push(this.getValue(item));
                }
            }
            return result;
        }
        if (target instanceof Map) {
            const result = new Map<any, any>();
            for (const [key, value] of target) {
                result.set(this.getValue(key), this.getValue(value));
            }
            return result;
        }
        const result = Object.create(Object.getPrototypeOf(target));
        for (const key of Object.getOwnPropertyNames(target)) {
            const source = target[key];
            const value = this.ignoredKeys.has(key) ? source : this.getValue(source);
            if (value !== undefined) {
                result[key] =  value;
            }
        }
        return result;
    }
}
function asArray<T>(target: T | Array<T>): Array<T> {
    return Array.isArray(target)
        ? Array.from(target)
        : [ target ];
}
const replacerToJson = (value: XmlParserResultValue ) => {
    if (typeof(value) !== "object") {
        return value;
    }
    if (value === null) {
        return null;
    }
    if (Array.isArray(value)) {
        return RmsArray.from(value);
    }
    const rmsMap = value as Partial<RmsDtoMap>;
    if (rmsMap.k !== undefined && rmsMap.v !== undefined) {
        const keys = asArray(rmsMap.k);
        const values = asArray(rmsMap.v);
        const d = new RmsMap<DefinedObject, DefinedObject>();
        for (let index = 0; index < keys.length; index++) {
            d.set(keys[index], values[index]);
        }
        return d;
    }
    const rmsArray = value as Partial<RmsDtoArray>;
    if (rmsArray.i !== undefined) {
        return asArray(rmsArray.i)
    }
    const rmsObj = value as Partial<RmsDtoObj>;
    if (rmsObj.__attributes !== undefined) {
        const attributes = rmsObj.__attributes;
        if (attributes.null) {
            return null;
        }
        const tagValue = rmsObj.__value;
        if (tagValue !== undefined) {
            return createRmsObj(attributes, true, { value: tagValue })
        }
        const r = createRmsObj(attributes, false, rmsObj);
        delete r[attributesKey];
        return r;
    }
    if (getRmsProto(value as any)) {
        console.log();
    }
    return Object.assign({ }, value);
}
const XmlProcessor = new ObjectProcessor(replacerToJson, [ valueKey, attributesKey ]);
const replacerToXml = (value: any) => {
    if (value === undefined) {
        return undefined;
    }
    if (typeof(value) === "object") {
        if (Array.isArray(value)) {
            return { "i": value };
        }
        if (value instanceof(Map)) {
            const r = {
                "k": new Array(),
                "v": new Array()
            };
            for (const [key, val] of value as Map<any, any>) {
                r.k.push(key);
                r.v.push(val);
            }
            return r;
        }
        if (value === null) {
            return {
                [attributesKey]: { null: 1, }
            };
        }
        const proto = getRmsProto(value);
        if (proto !== null) {
            return proto.byValue
                ? { [attributesKey]: proto.attributes, [valueKey]: value.value }
                : Object.assign({ }, value, { [attributesKey]: proto.attributes });
        }
    }
    return value;
}
const JsonProcessor = new ObjectProcessor(replacerToXml);
function measure<T>(name: string, func: () => T): T {
    const t0 = new Date().valueOf();
    const r = func();
    const t1 = new Date().valueOf();
    const dt = t1 - t0;
    console.log(`${name}: ${dt} ms`);
    return r;
}
const nextGen = {
    key: "__rmsObj",
    cls: "__cls",
    eid: "__eid",
    value: "__value",
}
function toNextGenJsonString(value: any): string {
    return JSON.stringify(value, (_, value) => {
        if (typeof(value) !== "object" || value === null) {
            return value;
        }
        if (value instanceof(RmsArray)) {
            return Array.from(value);
        }
        if (value instanceof(RmsMap)) {
            return {
                rmsObj: "map",
                values: Array.from(value),
            }
        }
        const t = getRmsProto(value);
        if (t === null) {
            return value;
        }
        return {
                [nextGen.key]: "object",
                [nextGen.cls]: t.attributes.cls,
                [nextGen.eid]: t.attributes.eid,
                [nextGen.value]: t.byValue ? value.value : Object.assign({}, value),
        };
    }, 2);
}
function parseNextGen(str: string) {
    return JSON.parse(str, (_, value) => {
        if (typeof(value) !== "object" || value === null) {
            return value;
        }
        const rmsKey = value[nextGen.key];
        if (rmsKey === undefined) {
            return rmsKey;
        }
        if (rmsKey == "map") {
            return new RmsMap(value.values);
        }
        if (rmsKey === "object") {
            const attr = { null: 0 } as RmsDtoAttributes;
            if ([nextGen.cls] !== undefined) {
                attr.cls = value[nextGen.cls];
            }
            if ([nextGen.eid] !== undefined) {
                attr.eid = value[nextGen.eid];
            }
            return createRmsObj(attr, typeof(value[nextGen.value]) !== "object", value[nextGen.value]);
        }
    });
}
function test() {
    const dataDir = "test-data";
    if (!fs.existsSync(dataDir)) {
        fs.mkdirSync(dataDir);
    }
    const d = fs.readFileSync(path.join(process.cwd(), "src", "data.xml"))
        .toString();
    if (parser.validate(d) === true) {

        const raw = measure("parsing xml", () => parser.parse(d, xmlToJsOptions));
        fs.writeFileSync(path.join(process.cwd(), dataDir, "source xml.xml"), new parser.j2xParser(jsToXmlOptions).parse(raw));

        const processed = measure("process xml", () => XmlProcessor.process(raw));
        fs.writeFileSync(path.join(process.cwd(), dataDir, "source json.json"), JSON.stringify(processed, undefined, 2));

        const nextGenStr = measure("serialize to nextJson", () => toNextGenJsonString(processed));
        fs.writeFileSync(path.join(process.cwd(), dataDir, "nextGen.json"), nextGenStr);
    
        const rawNextGen = measure("parsing nextJson", () => parseNextGen(nextGenStr));
        fs.writeFileSync(path.join(process.cwd(), dataDir, "parsedNextGen.json"), JSON.stringify(rawNextGen, undefined, 2));

        const processedXml = measure("processing xml", () => JsonProcessor.process(processed));
        fs.writeFileSync(path.join(process.cwd(), dataDir, "processed json.json"), JSON.stringify(processedXml, undefined, 2));

        const xmlString = measure("serialize xml", () => new parser.j2xParser(jsToXmlOptions).parse(processedXml));
        fs.writeFileSync(path.join(process.cwd(), dataDir, "result xml.xml"), xmlString);

    }
}
test();